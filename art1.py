# Syriana Poteat
# 03.15.2023
# this will have all of the character art in it to be displayed in the game

# demo stuff:
#logo = """

#logo2 ="""

#"""


enemy_list = {"skelleton":
"""
                              _.--""-._
  ^                         ."         ".
 / \    ,^.         /(     Y             |      )\
/   `---. |--'\    (  \__..'--   -   -- -'""-.-'  )
|        :|    `>   '.     l_..-------.._l      .'
|      __l;__ .'      "-.__.||_.-'v'-._||`"----"
 \  .-' | |  `              l._       _.'
  \/    | |                   l`^^'^^'j
        | |                _   \_____/     _
        j |               l `--__)-'(__.--' |
        | |               | /`---``-----'"1 |  ,-----.
""",
    "dragon":
"""
                                             ,--,  ,.-.
               ,                   \,       '-,-`,'-.' | ._
              /|           \    ,   |\         }  )/  / `-,',
              [ ,          |\  /|   | |        /  \|  |/`  ,`
              | |       ,.`  `,` `, | |  _,...(   (      .',
              \  \  __ ,-` `  ,  , `/ |,'      Y     (   /_L\
               \  \_\,``,   ` , ,  /  |         )         _,/
                \  '  `  ,_ _`_,-,<._.<        /         /
                 ', `>.,`  `  `   ,., |_      |         /
""",
"woods":
"""
▒▒▒▒▒▒▒▒▓▒▒▒▒▒▓▒▒▒▒▒▒▓▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒█▒░░▓▒█ ░█▓░ ▒█▒░▓▓█▓▒▒▒▒▒▓░▒▒▓▓▓█░░▓▓▓▒▓▓▒▓▓▒▓▓▓░░▓█▓▒██▒
▒░ ▒▒▓▓▓▓   ░▓▓ ░░░▒▓▓▒▒▒▓█▓▓████▓▓▓▓▒▒▒▒▓█░░▒▓▓▓ ░▓░ ░▒█░░▓▓█▒░░░░░ ░░░▓███░░░░▒▒▓▓▒▒▒▒▓▓▓▓▒▓▓▓▓▓▓▓
▓░ ▒▒▒▓▓▓░░▒▒▓▓░▓░▒░▓▓▒▓▒▓▓▓▓▓▓█▓▓███▒▓▒▓▓▓░▒▒▓█▒ ░▒▒▒▒▒▓░▒▓▓█▒░▒░░░▒▒░▒▒▓▓█▒▒▓▒▓▒░ ▒▓▓▓▒░▒▓█▓░▒█▓▓▓
▓░ ▒▒▒▒▓▒░▒▒░▓▓▒▒░▒░▓▓░▓▒▓▒▓▓▒▓▓▒▒▓▓▓▒▓▒▓▒▒░▓▒▒█▒ ░██▒ ▓▓▒░▒▒█▒░▒▓▓▒░▒░▒▒▒▒██▒▒▓▓▒▒▓▓▓▓▒░▓▓▒▒██▓▒▓██
█░ ▒▒▒▓▓▒▒▒░▒▓▓░░░▒▒▓▒▒▒▓▓▒▒▓▒▓▓▓░▒▓▓▒▒▒▒▒▒░▒▒▓▓▓░▒▓░ ▒▓▓▓░░▓█▓░▒▓▒░▒░░░▒▒▓▓█▒▒░▒▒▓▒▒▒▒░▒▒▒▓▓▓██▓▒▒▓
█░░▒▒▒▓▓▒░░▒▒▓▓▒▒░▒▒▓▓▒▒▓▓▒▒▓▒▒█▓▒▒▓█▓▒▒▒░░░▒░▒▒▒ ░▒░▓▒░▓▒░▒▓█▓▒░▒▓▓░▒▒░▒▒░▓▓░▒▓▒▒▒▒░▓▒░▒▓▓▒▒▓▓▓██▓▓
█░░▒▒▒▓▓▒░▒░▒▓▓▒░░▒▒▒▒▒▒▒▓▒░▓▒▒▓█▒▒▓█▓▒▒▒░░░▒░▓▒▒░▒▒▒▒ ░▓░░▒▓█▒ ░▒▒░▓▒▒░▒▒▒▓▓░▒▒▒░▒▒▓▓▒▒▓▓▒▒▓▓▓▓▓▓█▓
█░░▓▒▓▓▓▒▒▒░▒█▓░  ▒▒▓░▒▓░█▒░▒▓▒▒█▒▒██▓▒▒▒▒░ ▒░▓▓▓ ▒▓▒░░▒▒▒░▒▒█▒▒▓░▒▓▓░░▓▓▒░▓▓▒▒░▒▒▓▒▒▒▒▒▓▒▓▓▓▓▓▓▒▒▒▓
█░▓▒▒▓▓▓▒▒▒░▒█▓   ▒▒▓░▒▓▒▓▓▒▓▓▒▒█▒▒█▓▒▒▓▒▒░░ ░▒▓░░▓▓░▓▓▒▒▒░▒▒█▒▒▓▒▒▒▒▓▒▓█▒▒█▓▒▒▒▓▒░ ▒▓▒░▒▓▓▓▓▓▓▒▒▓▓░
▓▒▓░▒▒▒▓▒▒▒░▓█▒   ░▒░▒▒▓▒▓▓▒▓▒▒▓█▓▒▓▒░░▓▒░ ░ ░▒▒ ░▒▒▒▓▒░░▒▒▒▒█▓░▒▒▒▓▓▓▒░▒▒▓█▒░▒▒░▒▒▓▓▒▒▒▓▓▓▓▒▓▓▒▒▓▓░
▓▒▒░▒▒▒▓▒░  ▓█▒ ░ ░▒░▒▒▓▓▓▓▒▓▒▓▓█▓▒▓▒ ░▒█░   ░█▒  ▓▒░ ░▒░▒▓░▒█▒ ░▒▓▓▓▒░ ░▒▓▓  ░░▓▓▓░▒▓▓░▒▒▓░ ▒▓░▒▓▓░
▓░░▒▒▒▓▓░░ ░▒█▓   ░▒▒░▒▓▓▒▒▓▒▒▒▒█▓▒▓▒ ░░█▓░ ░▒█▒  ▒▒  ░░░▓▓░▓█▒░▒▒▓░░░░▒▒ ▓▓  ░▒▒▒░ ▒▓▒▒▓▓▓░ ▓▒▒▒██░
▓ ▒▓▒▓▓█▒  ░▓█▓ ░ ░▓█░▒▓▒▓▒█▓▒▓▒█▓▒▓▓  ▒▓█▓ ░▓▓▒  ▓▒     ▒▓░▒▓  ▒▒▓   ░▓▒░▓▓  ░ ▒▓░ ░░░ ░▒▓  ▓▒▒▓▓█░
▒ ▒▒▒▓▓█▒  ░▓▓▓   ░▓█▒▒█▒▓▒▓▓▒▒▓█▓▒▒▓░ ░▒█▓░ ▓█▓  ▓▓   ░▒▒▒▒▒█  ░░▓   ░░▒▒▒▓    ░▓   ▒░ ░▒▓  ▒▓░▒▓█▒
▒ ▒▒▒▒▓▓▒  ░▓▓▓   ▒▒█▒▒▓▓▒▓▒▓▓▒▓█▓▓▓▓▒ ░▓██░ ▒▓▒ ░▓▓   ░▒▒▒░▓█ ░░░▓   ░ ▒░▒▓    ▒▓  ░▒░ ░▒█░ ░▒▒▒▓█▒
▒ ▒▒▒▓▓▓▓░ ▒▓▓█░  ▒▒█▒▒▓▓▒▓▒▓▓▒█▓▒▒▓▓▒ ░▓█▓░ ▒▓▒ ░░▓   ░▒█▓▒▒▓ ░ ▒▒   ░░▒░▒▒    ▒▓  ░▒░  ▒█░ ░░▓▒▓▓▓
▒░▒▓▒▓▓▓▓░░▓▓▓█▒░░▒░▓▓▒▓█▒▓▒▓▒▒█▓▒▒▓▓▒▒▒██▓  ▒▓▒░▒▒▓▓▒░░▓▓█▒▓▓░░▒▒▓▒▒░ ░▒▓▒▒   ░▓▓░ ░▒▒  ▒█░ ░▒▒▒▓█▓
▒░▒▓▒▓▓▓▓▒░█▓▓██▒░▒░▓█▒▒█▒▒▓▓▓░██▒▒▓░▓▓▒██▒ ░▒▓░ ░░░▒▒░▒░░▓░▓▓   ░░▒▒▒ ░▒▒▒▒ ░░▒▒▓▒▒▒▓█░ ▒█▒░▒▒▒▒▓▓▓
▒░▒▒▒▓▓▒▓▓▒▒▒▒▓▓▓▓▓ ▓█▒▒█▒▒▓▓▓░▓█▓▒▓░▒▓░█▓░ ▒▒▓░ ░░░░ ░░░░▒▒▓▒░░░░░    ░▒░▓▒░░░░░░░░▒░▒░ ▒▓▒░░▓▒▓▓█▒
░ ▒▓▒▓▓▓▓▓▒▒▒▒░░░░▒░▓█░▓█▓▒▓▓▓▒▒█▓▒▓░░▓░█▒ ░▓▒▓░ ░░░░░░░░▒░░▓▒     ░░  ▒▒▒▓▒ ░░░░░ ░ ░░░░▓▒▒▒░▒▓▓██▒
░ ▒▓▓▓▓▓▓▓▒░░░░░░ ░▒█▓▒▓▓▓▒▓▒▓▓▒██▓▒▒ ▒░▓▒ ▒▒░▓░      ░░░▒▒▓▓░     ░░ ░▓░░█▓   ░░░░░░▒▒░▒▓▓▓▓░▓▓▒▓█▓
 ░▓▓▓░▓▓▒█▓░░░░░░░░▒█▒▒▓▒▓▒▓▒▓▓██▓▓░▒▓░ ▒░░░▒▓█▒       ▓░▒▓▓▓░ ▒▓▓▒░░▒▒▒▓░▒▓▓▒░            ░▒░▓▓▒▓▓▓
 ▓▓▓▒▒█▓▒▒█▓▒▒▒▒▒▒▒▒█▒▒▓▓▓▒▓▓▓▓██▓▓░▒▓▒ ▒▓ ░▓██░      ░▒░▒▓▒█▒ ░░░░░▒░░░▓▓▓▒▒▓▓▒▒▒▒░▒░░░    ░░█▓▓▓█▒
▓▓▓▓▒███▓░▓█▓▒▒▒▒▒░▒▓▒▒▓▓█▒▓▒█▓█▓▓▓▓▒▒▓░░█▒░▓██░ ░░░  ░▒ ░░░▓░             ░░▒▒▒▒▓▒▒▒▒▒▒▒▒░░▒▓▒▓▓▓▓▓
▓▒▓▓▓▓█▓▓▓▓▓█▓▓▓▓▒▒░▓▓▒▓▓▓▒▒▓█▓█▓▓▓▓▒░▓░ █▓░▒▓█░░░▒▒░░▒█▒ ░░▒▓                    ░░░░░░░░░▒▓▒░▒▓█▓▒
▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▓▒▒█▓▒█▓▓▓▒▓▓▓█▓▓▓█▒ ▓▓ ▒█░▓▓█░ ░░░ ▒ ▒░░▒▓▒█▓▒▒▒▒▒░░░░                ░▒▓▓▒▒▒▓▒▓█▓
▓▒▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒█▓░█▓▓▓▓▓▒▓█▓▓▓█▓░▓█░░█░▒██     ░▒ ░█▓ ▓▓▒▒░░░░▒▒▒▒▒▒▒░░░░░         ░▒▓▒▒▓▓██▒▓█
▒░▒▒▒▒▒▒▒▒░▒▒▒▒▓▒▒▒▒█▒░█▓▓▓▓▓▒▓█▓▓░▓█▓██░░█▒░▓█░ ░░▒▓░░█▓▓░░▓▓░░░     ░░░░░▒▒▒▒▒▒▒░░░▒▒▒▒▒░▒▒▓▓▓▓█▒▒
▒▒▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▓█░▒█▓▓▓▓▓▒▒█▓▓▒░███▓▒░█▒░▓▓▓░▒▒▒▓▓▓▒▒▒▓▒▒▓▒░░▒▒░          ░░░░░░▒▓▓▒▓▓▓▓▓▒▒▒▒▓▓▒
▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▒▒▒▓▒ ▓█▓▓▓▓▓▒▓█▓▓▓░░███▒▒█░ ▓▒▓▒░░░▒▒▒░▒▒▒▒▒▒▒▒▒░▒▒▒▒░              ░ ░░▒▒▒▒▓▓█▓█▓▓
░░░░░░░░░▒▒▒▒▒▓▒▒▒▓▓░░▓█▓▓▓▓▓▒▓██▓▓▓░▓██░▒▓░░▓▒ ▓▒░░░░░  ░░░░░▒▒▒▒▒▒▒▒▒▒▒▒░░░              ░░░▒▒▓▓▓▓
░ ░░░░░░░░░░░░▒▒░▓▓▒░░██▓▓▓▓▒▒██▓█░▒░▒███▓░░▓▒█▓░▓▒░ ░░░░░░   ░░░░░░░▒▒▒▒▒▒▓▓▓▓▒▒░░            ░░▒▓▓
░░░░░░░░░░░░░░░ ░▓▒▒ ▒█▓█▓▓▓░▓██▓██░ ░██▓▒▓ ░▒▒█▓░▓▓░  ░▒▒▒░░░░░      ░░░░░░░░▒▒▒▓▓▓▓▒▒░░░░ ░░░░░ ░▒
▒▒▒▒▒▒░░░░░░░░░ ▒█▓░░██▓▓▓▓▒░████▓▓▓░░██▒ ▓▒  ▒▓██▒▒▓▒       ░░░░▒░░░░░░            ░░░░░▒▒▒▓▓▓▓▒▒▒▒
▒▒▒▓▓▒▒▒▒▒▒░░░░ ▓█▓▒▓█▓▓▓▓▓░▒█████▒▒▒░██▓ ░█▓░░░███▓▒▓▓              ░▒░▒░░░░░░░░           ░░▒▒▒▓▓▓
▓▒▒▒▒▒▒▒▒▓▓▓▓▒▒▓▓▓▒▓▓▓▓█▓▓░░███████▒░▒▓██░░███▓ ░████▓▓▓                 ░░░▒▒▒▒▒▒▒▒▒▒░░░░░░   ░ ░░░
▓▒▒▒▒▒▒▒▒▒▒░▒▒▓▓▒▒▓▒▒▓██▒░ ▒███████▓░▒▒▒█▓░░▓███ ░▓███▓▓▒                      ░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒░▒░
▒▓▓▓▒▒▒▒▓▓▓▓▓▒▓▓▒▓▓▒▓█▓░ ░▓██▓██████▓▒░░▓█▒  ▒███  ░▒██▓█                               ░░░▒▒▒▓▓▓▓▓▓
▓▒▓▒▓▓▓█▓▓▓▒▓▓▓▒▓▓▒▓██▒  ▒██▓████████▓▒░ ▓█▒░ ▒███░  ▒█▓█▒                                   ░░░▒▒▓▒
▓▓▓▓▓▓▒▒▒▓▓█▓▓░▒█▒▒██▓▒░▓██▓▓█▓███████▒░░░██▒  ████▒  ▒▓▓█▒░ ░ ░░    ░░░░░░    ░░░░░   ░ ░░  ░░░░░░░
▓▓▓▓▒▒▓███▓▒░░▒▓▒▒██▓▒▓███▓▒▓██████████▓▒░░▓█▓▒▒▒▒▓██░ ░▒▒▓█▓▓▒▒░░░░  ░░░░░ ░░░ ░░▒░░▒▒▒▒▒▒▒▒░░░▒▒▒▒
▓▓▓▓▓▓▓▓▒▒▒▒▓██▓▓█▓▒▒▓█▓▓▒░▓▓▒▒▒▓▓▓▓▓▓███▒░░▒███▒▒▒▒▓▓▒░░░░░▒▒▒▒▒▒▒▒░░░░░░░░░░░░░░░ ░░░░░▒▒▒▒▒▒▒▒▒▒▒
""",
"chapter1":
"""
░ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░░░ ░░░░░ ░░░░░ ░ ░ ░   ░                                 ░░░░▒▒▒▒░░░         ░
░                                                                                                                          ░░░░░░░
░ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░ ░░░░░░░ ░░░ ░ ░ ░ ░ ░   ░               ░░░░░░▒▒▒▒░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░░░ ░░░░░░░░░ ░ ░░░ ░ ░░░ ░ ░ ░ ░            ░░░░░▒▒▒▒▒░░░░░░
░ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░░░ ░ ░ ░ ░             ░░░░▒▒▒▒▒▒▒▒▒░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░ ░░░ ░ ░ ░ ░           ░░░░▒▒▒▒▒▒▒▒▒░░░░
░ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░ ░ ░ ░             ░░░░▒▒▒▒▓▒▒▒▒░░░░             ░ ░ ░       ░
░ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░          ░░░▒▒▒▒▒▒▒▒▒▒▒░░░░             ░ ░ ░ ░     ░   ░ ░
░ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░         ░░░░▒▒▒▒▒▒▒▒▒▒▒░░░            ░ ░ ░ ░ ░ ░ ░ ░ ░ ░ ░   ░ ░
░ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░        ░░░░▒▒▒▒▒▒▒▒▒▒▒▒░░░        ░ ░░░ ░ ░ ░ ░░░ ░░░ ░ ░ ░ ░ ░ ░ ░ ░ ░ ░░░   ░
░ ░░░░░░░░░░░░░░░░░░░░░░░ ░     ░░░░▒▒▒▒▒▒▓▒▒▒▒░░░░           ░ ░░░░░░░░░ ░░░ ░░░░░░░░░ ░ ░ ░░░░░░░ ░ ░ ░ ░ ░   ░     ░     ▒█▓
░░░░░░░░░░░░░░░░░░ ░ ░░░░░░▒▒▒▒▒▒▒▒▒▒▒░░░░         ░ ░░░░░░░░░ ░ ░░░░░░░░░░░░░ ░ ░░░░░░░░░ ░░░░░░░ ░ ░ ░ ░ ░ ░ ░ ░ ░      ░████▒
░ ░░░░░░░░░ ░░░░░░▒▒▒▒▒▒▒▒▒▒▒░░░░       ░░░ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░░░░░░░ ░ ░ ░ ░ ░ ░ ░ ░ ░ ░   ░     ░░  ██▒
░ ░░░░░░░▒▒▒▒▒▒▒▒▒▒▒░░░░   ░  ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░░░░░░░░░░░░░░░░░░░░░ ░░░░░░░ ░ ░ ░ ░     ░        ░██░
▒▒▒▒▒▒▒▒▒▒▒░░░░░░   ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░░░ ░ ░ ░ ░ ░ ░ ░                  ▒██
▒░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░     ░░░░░░░          ░ ░▓▓░   ░        ▓█▓
░ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░  ▓   ░ ░ ░   ░░░    ▒█░████▒ ░   ░ ░    ▓█▓
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░█▒        ▓█▓▓██▒ ░███▓ ▓█▒  ░░  ░ ░    ██▒
░ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░   ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░     ░  ▓██▒▒██░  ██░   ▒██▓▒██▓     ░░  ░ ░     ░██░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░   ░█▒ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░             ▓█████▓▓░ ▒█▓    ▒███  ██▒   ░ ░ ░ ░ ░ ░   ▒██
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░         ░▒▓███  ░░░░░░░░░░░░░░░░░░░   ░░░░  ▓   █████▓░ ░░▒██▒    ▓██▒▓██▓▒░░  ░██▒   ░ ░ ░ ░   ░   ▓█▓
░░░░░░░░░░░░░░░░░░░░░░░░░░░    ░▒▓▓▓▓▓▒░▒█████▒  ░░░░░░░░░░░░░░░           ▒█▒ ████████▓   ▓██░   ▒███░         ▓██░  ░░ ░░░ ░ ░     ███▒▒░
░░░░░░░░░░░░░░░░░░░░░░░░░   ▒████▓▓▓████▒  ▓███░ ░░░░     ░░░░░  ▒▓████▒  ▓████▒    ▒████░  ███    ███░   ░ ░ ░  ███  ░ ░ ░ ░ ░ ░   ▒█▓▒▒▒░
░░░░░░░░░░░░░░░░░░░░░░░░ ░▒██▓░      ░▒██▒ ░███▓  ░   ░░░   ░░ ░██▒░░███▓░█████       ▒███▒ ▒██▓   ▒███░   ░░░   ░██▓    ░ ░ ░ ░ ░  ░
░░░░░░░░░░░░░░░░░░░░░░░ ▒███▒  ░░░░░░   ▓█░ ▓███░  ░▓█████▒    ██▓    ███░  ███▓  ░░░  ░███░ ▓██▒   ▓███░   ░   ░ ░██▒ ░░ ░░░░░ ░░░         ░
░░░░░░░░░░░░░░░░░░░░░░ ▒███▓  ░░░░░░░░░  ░░  ████ ░███▓█████░ ░███ ░  ▒███  ░███▒ ░░░░  ▒███  ███░   ▒███▓     ░█░ █████▒ ░░░░░░ ░ ░ ░░  ░   ░     ░
░░░░░░░░░░░░░░░░░░░░░ ░████░ ░░░░░░░░░░░░░░░ ▒███▓█▒    ░████ ░██▒ ░░  ▓██▓  ▒███   ░░░  ▓██▒ ▒███    ░████▓▓▒▓█▓ ▓▓░░    ░░░░░░░ ░   ░ ░ ░ ░       ░
░░░░░░░░░░░░░░░░░░░░░░▓███▓  ░░░░░░░░░░░░░░░  ████▓  ░░  ░███▓ ░░  ░  ░████░  ▓██▓  ░░░░ ░██▓  ████  ░▓ ▒██████▓       ░ ░░░ ░░░ ░ ░ ░ ░       ░
░░░░░░░░░░░░░░░░░░░░░░████▓ ░░░░░░░░░░░░░░░░░ ░███▓  ░░░░ ▒███▒  ░░  ▒█▒▒██▓  ░███▒ ░░░░  ██▓   ██████▓    ░░░    ░ ░░░░░░░░░░░░░░░ ░ ░░░ ░ ░
░░░░░░░░░░░░░░░░░░░░ ▒████▓  ░░░░░░░░░░░░░░░░  ▓███░ ░░░░  ████  ░  ▓█░  ███▒  ▒███  ░░  ░██░    ▒▓██▒         ░░░░░░░░░░░░░░░░░ ░ ░ ░ ░ ░       ░░▒▒▒
░░░░░░░░░░░░░░░░░░░░░▒█████░ ░░░░░░░░░░░░░░░░░ ░███▓  ░░░░ ░███▓   ▓█▓   ░███   ▓███░    ██▓  ░░       ░░░░░░░░░░░░░░░░░░░░░░░░ ░░░░░░░      ░░▒▒▒▒░
░░░░░░░░░░░░░░░░░░░░ ▒█████▒ ░░░░░░░░░░░░░░░░░░ ▒███▒ ░░░░  ▓███░ ░███    ████  ▓█████▓▓██▒  ░░░░░ ░ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░     ░▒▒▒▒▒░
░░░░░░░░░░░░░░░░░░░░░░██████░ ░░░░░░░░░░░░░░░░░  ████░ ░░░░ ░████ ░███▓  ▒█▓████▓▒███▒▒▒░   ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░     ░░▒▒▒▒▒░      ░ ░
░░░░░░░░░░░░░░░░░░░░░░▒█████▒ ░░░░░░░░░░░░░░░░░░ ░███▓ ░░░░░ ▒███▒ ▓██████░ ░███  ▓██▓   ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░    ░░▒▒▒▒░░     ░ ░ ░ ░
░░░░░░░░░░░░░░░░░░░░░░░▓█████░  ░░░░░░░░░░░░░ ░█░ ▓███░ ░░░░  ▓███▒░█████░        ░███▒  ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░   ░░▒▓▓▒▒░      ░ ░ ░ ░░░░░
░░░░░░░░░░░░░░░░░░░░░░ ░▓█████░ ░░░░░░░░░░░░░ ▒█▓ ░███▓   ░░ ▒██████▒░▒▒   ░░░ ░░░ ▒███░   ░░░░░░░░░░░░░░░░░░░░░░░░░    ░▒▒▓▓▒░░   ░ ░░░░░░░░░ ░ ░ ░░
░░░░░░░░░░░░░░░░░░░░░░░ ░▓█████▒  ░░░░░░░░░░░ ▒██  ▓████▓▓▒ ▒██▒░        ░░░░░░░░░  ▓███░ ░░░░░░░░░░░░░░░░░░░░░░    ░▒▒▓▓▒░░    ░░░ ░░░░░ ░░░░░░░ ░ ░
░░░░░░░░░░░░░░░░░░░░░░░░ ░▒█████▒░ ░░░░░░░░░  ███▒░███████▒░░     ░░░░░░░░░░░░░░░░░ ░██████▒ ░░░░░░░░░░░░░░░   ░░▒▒▓▓▒░░   ░░░░░░░░░░░░░░░░░░░░░ ░ ░░
░░░░░░░░░░░░░░░░░░░░░░░░░  ░▓█████▒   ░░░░░  ▓██▓░██▒░░   ░░░░░░░░░░░░░░░░░░░░░░░░░▓██▓▒▒░░░░░░░░░░░░░░░   ░░▒▓▓▓▒░░  ░ ░░░░░░░░░░░░░░░░░░░░░ ░░░ ░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░  ▒▓█████▓▒░░░░░▒██▓░  ░    ░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒      ░░░░░░░░░   ░▒▒▓▓▓▒░░   ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░  ░▒▓██████████▒░  ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░   ░░░░░░░░░   ░░▒▒▓▓▒▒░   ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░  ░░░▒▒▒▒░░░ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░  ░░▒▓▓▓▒▒░░  ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░     ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░   ░░▒▓▓▓▒░░   ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░  ░░▒▒▓▓▒▒░░  ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░ ░░░▒▓▓▓▓▒░░ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
"""
}
