# syriana Poteat
# 03.11.2023
# this will generate random enemy names
# The function will take “levelBoss” as an argument.
#This boolean variable will let us know if the enemy we are creating is a
# boss (True) or a common enemy (False)

import random
with open('adjective.txt', 'r') as file:
  lines = file.readlines()
  adjective = lines[random.randint(0, len(lines)-1)][:-1]
with open('animal.txt', 'r') as file2:
  lines2 = file2.readlines()
  animal = lines2[random.randint(0, len(lines)-1)][:-1]

if levelBoss == False:
  health = random.randint(50, 100)
  attack = random.randint(5,10)
  special = random.randint(10,20)
  chance = random.randint(1,10)

# in here it's saying: call the class Enemy and give those attributes
  return Enemy(health, attack, special, chance, adjective + " " + animal)

else:
  health = random.randint(200, 250)
  attack = random.randint(20, 40)
  special = random.randint(50, 60)
  chance = random.randint(1, 8)
  superMove = random.randint(100,200)
  # return the Boss object and give it those attributes
  return Boss(health, attack, special, chance, adjective + " " + animal, superMove)
